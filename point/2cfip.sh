#!/bin/bash
#
# Encrypted by Rangga Fajar Oktariansyah (Anak Gabut Thea)
#
# This file has been encrypted with BZip2 Shell Exec <https://github.com/FajarKim/bz2-shell>
# The filename '2cfip.sh' encrypted at Tue Mar 12 13:39:30 UTC 2024
# I try invoking the compressed executable with the original name
# (for programs looking at their name).  We also try to retain the original
# file permissions on the compressed file.  For safety reasons, bzsh will
# not create setuid or setgid shell scripts.
#
# WARNING: the first line of this file must be either : or #!/bin/bash
# The : is required for some old versions of csh.
# On Ultrix, /bin/bash is too buggy, change the first line to: #!/bin/bash5
#
# Don't forget to follow me on <https://github.com/FajarKim>
skip=75

tab='	'
nl='
'
IFS=" $tab$nl"

# Make sure important variables exist if not already defined
# $USER is defined by login(1) which is not always executed (e.g. containers)
# POSIX: https://pubs.opengroup.org/onlinepubs/009695299/utilities/id.html
USER=${USER:-$(id -u -n)}
# $HOME is defined at the time of login, but it could be unset. If it is unset,
# a tilde by itself (~) will not be expanded to the current user's home directory.
# POSIX: https://pubs.opengroup.org/onlinepubs/009696899/basedefs/xbd_chap08.html#tag_08_03
HOME="${HOME:-$(getent passwd $USER 2>/dev/null | cut -d: -f6)}"
# macOS does not have getent, but this works even if $HOME is unset
HOME="${HOME:-$(eval echo ~$USER)}"
umask=`umask`
umask 77

bztmpdir=
trap 'res=$?
  test -n "$bztmpdir" && rm -fr "$bztmpdir"
  (exit $res); exit $res
' 0 1 2 3 5 10 13 15

case $TMPDIR in
  / | */tmp/) test -d "$TMPDIR" && test -w "$TMPDIR" && test -x "$TMPDIR" || TMPDIR=$HOME/.cache/; test -d "$HOME/.cache" && test -w "$HOME/.cache" && test -x "$HOME/.cache" || mkdir "$HOME/.cache";;
  */tmp) TMPDIR=$TMPDIR/; test -d "$TMPDIR" && test -w "$TMPDIR" && test -x "$TMPDIR" || TMPDIR=$HOME/.cache/; test -d "$HOME/.cache" && test -w "$HOME/.cache" && test -x "$HOME/.cache" || mkdir "$HOME/.cache";;
  *:* | *) TMPDIR=$HOME/.cache/; test -d "$HOME/.cache" && test -w "$HOME/.cache" && test -x "$HOME/.cache" || mkdir "$HOME/.cache";;
esac
if type mktemp >/dev/null 2>&1; then
  bztmpdir=`mktemp -d "${TMPDIR}bztmpXXXXXXXXX"`
else
  bztmpdir=${TMPDIR}bztmp$$; mkdir $bztmpdir
fi || { (exit 127); exit 127; }

bztmp=$bztmpdir/$0
case $0 in
-* | */*'
') mkdir -p "$bztmp" && rm -r "$bztmp";;
*/*) bztmp=$bztmpdir/`basename "$0"`;;
esac || { (exit 127); exit 127; }

case `printf 'X\n' | tail -n +1 2>/dev/null` in
X) tail_n=-n;;
*) tail_n=;;
esac
if tail $tail_n +$skip <"$0" | bzip2 -cd > "$bztmp"; then
  umask $umask
  chmod 700 "$bztmp"
  (sleep 5; rm -fr "$bztmpdir") 2>/dev/null &
  "$bztmp" ${1+"$@"}; res=$?
else
  printf >&2 '%s\n' "Cannot decompress ${0##*/}"
  printf >&2 '%s\n' "Report bugs to <fajarrkim@gmail.com>."
  (exit 127); res=127
fi; exit $res
BZh91AY&SYD��� s_�0}����������w����������� }$�} �b   V�����$hCI��6��ڙM2i�� F�h  �� �L�0� �&�2`#2i�CF �d�&�� ba4��F��&L �&dCd�FL`FM2hh���Ѡ4dCL&�h�2dɀS��T �&�  �i�  h�  �C      "U4�M2�  h4@4          �R  ��FE<)����?R~�S�oSQ�{H���T��d�i�  �4 i���1���o�F��	{ޓ݆t�`}���X��� :���y��L��\��_#v-�M��9��O�R���h��uC%��e򠧜3�_6z�3JQ|D.K���J�tC�XcF�h�B�Ɯ�E]����!T�S{2:�eY���*@��4u岅L$�P �"#ԗ��uJ��Cck��?j���9��@ ��7�`}��v(=��v���zˏ'��3�l��C ���^d�i5�vaJ�a�Br��~�g�Z�a`�#J� l����5��xn�7�B�	�@l�����\;>g�U��NE�D��뮺�-�r.�%u�]u��mӑw"$���N�if�!�
=���t�!H�{�s?�I�ފg(C�������&o��*S�}3�$Q�l�\$2IUr�fT��9t`� �A�~oҧ�R���`�o*��۰1��w��9��W�*K��~��M&P�����h��-� N��x^�'h��+zP���}�J���3/L��������_Y��h���Lv#{���~9����ʽf�ɠ��P��߈���}����Gw�YJ�
C(A@�0O!�	�5C�X�Ӯ����ß���/�bB�`�I�e"IhoJ��$a��
ޮ���	 S�O=)/N��J��j^��d�A�N��yaU��h`�c1׽�{�@)�>i*���<��[Pnq���!�<�9�k\6q/d4�H�c(���p��}���9�s��w��iw�]�&���SvT�#�6;ݟ����)J��_� U�Pd�Y�S�P�
�A��BQ#
�/(�)R         JR��)J%�8�ks�cx�Z�V����`�5�v�1{�C�
��[@�r}T!�!��|sf!V=����[7cYTn���ց�x11��A A@r�B�C�A3
@�\DD����(�ZIƳ���H �[F �2E/d�DD�x��)�Hv2C �s��O�+C#�n�&v�2��!���������4�J��q��rO����g���V8s�jC4����[F�cc���l.&A�L��)+�IPg�!.-G���Z<����^�A�O�Y?8�u)\�&d��H�,�8�)��p��%�B�E����1�ȤN��'w<��+����!Gɞc�pǁ��E�b�qF�l�i/tʊ���� A����nH�����=�M�G�g��9wt��W]�)7�W��ј�q���mU����3�x��uw'�����T�`�+$!,�0c0c0c0c0c0c0c0cp"�a�4/A��ZE��+>�
t���]�N��=|��_�h����i�|�_<;���8�ĕn`蘺���bR��,WIka��$��p��%Eb�УР���hX|z���C1��#����)
�ӺRM7�[�`�Vْi���������	Z�Sg��OO�h]���ƀ~��� i՝Y�m��t�%kui�&���a�4�h�r�r��Qs�}� orPVt�|R�E���h�.*+��������a�2T!� �joc�*t�&h-y�6�ப�8H bb���F�>���k��P`��5qJW� ���e�R�2`7� .���U;N�*.�C����	�\�jC��5��C)���5�dSWM[?��x�p��˷[ HnI�bQz��R��!ˑF�KD:�B�F����)D�KL#@0*�J��I4K�r�%i���i�#Z��hfד�zL��TcJP��%�8Xݐ���0\D��&@�B�HL $��q��͠�,d���i�xK�CZ�������Fh�3��X�r�瞠��� "���F\B���!�>�B�>kSp�@H(07�����Wb`�H��iX/��Nَ\?��w��߆�`��xL���s��%(�"L@@Ȑ��3cVLk��PfM�|����nJ�01��c�N ���F.j7�A� � "9�\w��� `�	��jD�� ��8��EB<�B�\�ڷ�`=��e��\�CW�z���h�R�nG�9����8�K�n�ފ�1���PyB3d3�Q�J�^kM��g����n
`H�@�02��5�f6#r��/�5U%���?���~�K�yhyn/��V��ւ��Vd�r�$�FP�D%uB��(U�HPT�pZi!�T%WZ8%]7�1��<��4Oe:��D�.����r)�k�-@����1��j����Y3��1�@�r?[?�rhj�04_#r�����~�'��ba�wg�t���}�O�0��a���?`C�3��K�4�ٽŔu��6�렫)}�)ř���n��4�3��_�ؽ�
�D�T�:~�����S����1HA]G�U�!NX��$p�ڡ(���mfBі�2�ڏ=~$�;f��6�^;B|�W�+�\C�*q�,�,R®5�o��e�)K�t�J�Z����)��4��A�¢.S�fiP:9b4�U�/�V2�Zx��	RE�Sj�K��"EKD�.\zH��.#4U�ԉ����4U6���iq��
��cN̙�T���{_;�w�~ɦ��C��g�5�N���v���Pg��o�Z+<�Q0Z%��8���(�ƻ8Ta���"�(H"U�`�